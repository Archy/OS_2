#include "gnu-efi/inc/efi.h"
#include "Kernel.h"
//setting page directory address in cr3
extern void setCR3(UINT32 pdeAddr);

struct MemoryMap getMemoryMap();
void printMemoryMap(struct MemoryMap map);
UINT32 identityWholeMem(UINT32 pdFlags, UINT32 ptFlags);
void printPDE(UINT32 addr);

void setIdentityPaging()
{
	KERNEL.ST->ConOut->OutputString(KERNEL.ST->ConOut, L"Getting memory map\n\r");
	KERNEL.map = getMemoryMap();
	KERNEL.ST->ConOut->OutputString(KERNEL.ST->ConOut, L"Setting up paging\n\r");
	
	KERNEL.pageDir = identityWholeMem(0x0000001B, 0x0000001B);
	setCR3((UINT32)KERNEL.pageDir);

	KERNEL.ST->ConOut->OutputString(KERNEL.ST->ConOut, L"Identity paging set\n\r");
}

struct MemoryMap getMemoryMap()
{
	EFI_STATUS result;

	struct MemoryMap map;  
	map.mapSize = 0;    
	map.memoryMapPtr = 0;
	map.totalPages = 0;

	// Get the required memory pool size for the memory map
	result = KERNEL.BS->GetMemoryMap(&(map.mapSize), 
		map.memoryMapPtr, NULL, &(map.descriptorSize), NULL);
	if(result != EFI_BUFFER_TOO_SMALL)
	{
		KERNEL.ST->ConOut->OutputString(KERNEL.ST->ConOut, L"Getting memory map size failure\n\r");
		printHEX(result);
		return map;
	}

	while(TRUE)
	{
		//loop until memory map is obtained
		result = KERNEL.BS->AllocatePool(EfiBootServicesData, 
			map.mapSize, (void**)&(map.memoryMapPtr));
		if(result != EFI_SUCCESS)
		{
			KERNEL.ST->ConOut->OutputString(KERNEL.ST->ConOut, L"Allocating pool failure\n\r");
			switch(result)
			{
			case EFI_OUT_OF_RESOURCES:
				KERNEL.ST->ConOut->OutputString(KERNEL.ST->ConOut, L"EFI_OUT_OF_RESOURCES\n\r");
				break;
			case EFI_INVALID_PARAMETER:
				KERNEL.ST->ConOut->OutputString(KERNEL.ST->ConOut, L"EFI_INVALID_PARAMETER\n\r");
			break;
			}
			return map;
		}

		//get actual memory map
		result = KERNEL.BS->GetMemoryMap(&(map.mapSize), map.memoryMapPtr, &(map.mapKey), 
											&(map.descriptorSize), &(map.descriptorVersion));
		if(result == EFI_SUCCESS)
		{
			//portString("Memory map obtained successfuly\n");
			break;
		}
		else  //memory pool was to small. Increase mapSize and free current pool
		{
			portString("Pool to small\n");
			map.mapSize += map.descriptorSize;
			result = KERNEL.ST->BootServices->FreePool(&(map.memoryMapPtr));
		}
	}
	
	//count nr of pages in memory
	EFI_MEMORY_DESCRIPTOR* MemoryDescriptorPtr = NULL;
	UINTN DescriptorCount = map.mapSize / map.descriptorSize;
	UINTN i = 0;
	UINT32 lastAddr = 0;
	UINT32 pageSize = 4 * 1024;
	
	for(i = 0; i < DescriptorCount; i++ )
	{
		MemoryDescriptorPtr = (EFI_MEMORY_DESCRIPTOR*)(map.memoryMapPtr + (i*map.descriptorSize));
		if(lastAddr < pageSize*MemoryDescriptorPtr->NumberOfPages + MemoryDescriptorPtr->PhysicalStart)
		{
			lastAddr = pageSize*MemoryDescriptorPtr->NumberOfPages + MemoryDescriptorPtr->PhysicalStart;
		}
	}
	
	map.totalPages = (lastAddr+pageSize/2) / pageSize;
	
	//printMemoryMap(map);
	
	return map;
}

void printMemoryMap(struct MemoryMap map)
{
	/*
	typedef struct {
	UINT32                Type;
	EFI_PHYSICAL_ADDRESS  PhysicalStart;
	EFI_VIRTUAL_ADDRESS   VirtualStart;
	UINT64                NumberOfPages;
	UINT64                Attribute;
	} EFI_MEMORY_DESCRIPTOR;
	*/
	
	UINT32 currSize = 0;
	EFI_MEMORY_DESCRIPTOR* curr = NULL;
	UINTN DescriptorCount = map.mapSize / map.descriptorSize;
	UINTN i = 0;
	
	KERNEL.ST->ConOut->OutputString(KERNEL.ST->ConOut, L"Priniting map\n\r");
	for(i = 0; i < DescriptorCount; i++ )
	{
		curr = (EFI_MEMORY_DESCRIPTOR*)(map.memoryMapPtr + (i*map.descriptorSize));
		KERNEL.ST->ConOut->OutputString(KERNEL.ST->ConOut, L"\n\r");
		
		//memory types
		switch(curr->Type)
		{
		case EfiReservedMemoryType:
		  KERNEL.ST->ConOut->OutputString(KERNEL.ST->ConOut, L"EfiReservedMemoryType\t");
		  break;
		case EfiLoaderCode:
		  KERNEL.ST->ConOut->OutputString(KERNEL.ST->ConOut, L"EfiLoaderCode\t");
		  break;
		case EfiLoaderData:
		  KERNEL.ST->ConOut->OutputString(KERNEL.ST->ConOut, L"EfiLoaderData\t");
		  break;
		case EfiBootServicesCode:
		  KERNEL.ST->ConOut->OutputString(KERNEL.ST->ConOut, L"EfiBootServicesCode\t");
		  break;
		case EfiRuntimeServicesCode:
		  KERNEL.ST->ConOut->OutputString(KERNEL.ST->ConOut, L"EfiRuntimeServicesCode\t");
		  break;
		case EfiRuntimeServicesData:
		  KERNEL.ST->ConOut->OutputString(KERNEL.ST->ConOut, L"EfiRuntimeServicesData\t");
		  break;
		case EfiConventionalMemory:
		  KERNEL.ST->ConOut->OutputString(KERNEL.ST->ConOut, L"EfiConventionalMemory\t");
		  break;
		case EfiUnusableMemory:
		  KERNEL.ST->ConOut->OutputString(KERNEL.ST->ConOut, L"EfiUnusableMemory\t");
		  break;
		case EfiACPIReclaimMemory:
		  KERNEL.ST->ConOut->OutputString(KERNEL.ST->ConOut, L"EfiACPIReclaimMemory\t");
		  break;
		case EfiACPIMemoryNVS:
		  KERNEL.ST->ConOut->OutputString(KERNEL.ST->ConOut, L"EfiACPIMemoryNVS\t");
		  break;
		case EfiMemoryMappedIO:
		  KERNEL.ST->ConOut->OutputString(KERNEL.ST->ConOut, L"EfiMemoryMappedIO\t");
		  break;
		case EfiMemoryMappedIOPortSpace:
		  KERNEL.ST->ConOut->OutputString(KERNEL.ST->ConOut, L"EfiMemoryMappedIOPortSpace\t");
		  break;
		case EfiPalCode:
		  KERNEL.ST->ConOut->OutputString(KERNEL.ST->ConOut, L"EfiPalCode\t");
		  break;
		default:
		  KERNEL.ST->ConOut->OutputString(KERNEL.ST->ConOut, L"Other\t");
		  break;
		}
		//ATTRIBUTES
		printHEX(curr->Attribute);
		//address and size
		printHEX(curr->PhysicalStart);
		printHEX(curr->NumberOfPages);
  }
}

UINT32 identityWholeMem(UINT32 pdFlags, UINT32 ptFlags)
{
	EFI_STATUS result;
	UINT32 i = 0;
	UINT32 pageCounter = 0; 
	
	EFI_PHYSICAL_ADDRESS pdAddr = 0;
	EFI_PHYSICAL_ADDRESS ptAddr = 0;
	UINT32 pde = 0;
	UINT32 *currPDE = 0;
	UINT32 pte = 0;
	UINT32 *currPTE = 0;
	
	UINT32 pageSize = 4 * 1024;
	UINT32 pageGroupSize = 4*1024*1024; // page_size * entries per page_table
	
	struct MemoryMap map = KERNEL.map;
	
	KERNEL.BS->AllocatePages(AllocateAnyPages, EfiLoaderData, (UINTN)1, &pdAddr);
	currPDE = pdAddr;
	for(i=0; i<1024; ++i, ++currPDE)
	{
		if(i==512 || pageCounter <= map.totalPages) //i=512 video memory!!!
		{
			KERNEL.BS->AllocatePages(AllocateAnyPages, EfiLoaderData, (UINTN)1, &ptAddr);
			pde = ptAddr & 0xFFFFF000; //clear bites for flags
			pde = pde | pdFlags; //set flags: cache disabled, read/write, present, supervisor only
			*currPDE = pde;
			//set up page table
			currPTE = ptAddr;
			for(int j=0; j<1024; ++j, ++currPTE)
			{	
				if(i==512 || pageCounter <= map.totalPages)
				{
					pte = 0;
					pte = (i*pageGroupSize) + (j * pageSize);	//identity page address
					pte = pte & 0xFFFFF000; //clear for flags 
					pte = pte | ptFlags;
					//set up page table entry
					*currPTE = pte;
					
					++pageCounter;
				}
				else
				{
					*currPTE = 0;
				}
			}
		}
		else
		{
			*currPDE = 0;
		}
	}
	//printPDE(pdAddr);
	return pdAddr;
}

void printPDE(UINT32 addr)
{
	KERNEL.ST->ConOut->OutputString(KERNEL.ST->ConOut, L"Pages dir and tables:\n\r");
	UINT32 * currPDE = addr;
	for(int i=0; i<1024; ++i, ++currPDE)
	{
		if(*currPDE != 0)
		{
			printHEX(i);
			UINT32 pde = *currPDE;
			pde =  pde & 0xFFFFF000;
			printHEX(pde);
			UINT32 j = 0;
			UINT32 * currPTE = pde;
			for(j = 0; j<2; ++j, ++currPTE)
			{
				if(*currPTE != 0)
				{
					KERNEL.ST->ConOut->OutputString(KERNEL.ST->ConOut, L"\t");
					printHEX(j);
					UINT32 pte = *currPTE;
					//pte =  pte & 0xFFFFF000;
					KERNEL.ST->ConOut->OutputString(KERNEL.ST->ConOut, L"\t");
					printHEX(pte);
				}
			}
		}
	}
}

