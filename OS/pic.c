#include "gnu-efi/inc/efi.h"
#include "Kernel.h"
extern int outb(unsigned short port, unsigned char val);
extern void setCR3(UINT32 pdeAddr);
UINT32 tick = 0;

void remapPIC()
{
	//remap pic interrupts to start from 32nd postion
	outb (0x20, 0x11);
	outb (0xA0, 0x11);
	outb (0x21, 0x20);
	outb (0xA1, 0x28);
	outb (0x21, 0x04);
	outb (0xA1, 0x02);
	outb (0x21, 0x01);
	outb (0xA1, 0x01);
	outb (0x21, 0x0);
	outb (0xA1, 0x0);
	return;
}

void initTimer(UINT32 frequency)
{
	//value send to pit
	UINT32 divisor = 1193180 / frequency;
	
	// Send the command byte.
	outb(0x43, 0x36);
	
	// Divisor must be send as 8bits lower-higher data
	UINT8 low = (UINT8)(divisor & 0xFF);
	UINT8 high = (UINT8)( (divisor>>8) & 0xFF );

   // Sending divisor
	outb(0x40, low);
	outb(0x40, high);
}

void timerCallback(struct intStackTrace trace)
{
	//tick++;
	//if multitasking is enabled
	if(KERNEL.maxProc > 0)
	{
		if(KERNEL.currProc == KERNEL.maxProc + 1)
		{
			busy = 1;
			
			KERNEL.ST->ConOut->OutputString(KERNEL.ST->ConOut, L"\n\tSTARTING\n");
			KERNEL.processes[0].regs.eflags = trace.eflags;
			KERNEL.processes[1].regs.eflags = trace.eflags;
						
			//first switch
			KERNEL.currProc = 0;
			trace = KERNEL.processes[0].regs;
			
			if(trace.cs == 0x10)
				trace.ss = KERNEL.processes[KERNEL.currProc].pdAddr;
			else
			{
				KERNEL.tss.esp0 = KERNEL.processes[KERNEL.currProc].tssAddr;
				setCR3(KERNEL.processes[KERNEL.currProc].pdAddr);
			}	
		}
		else if(busy == 0)
		{
			busy = 1;
			//switch
			UINT32 prevCS = trace.cs;
			UINT32 next = KERNEL.currProc + 1;
			if(next == 2)
				next = 0;
				
			portString("\t\tsw\n");
				
			KERNEL.processes[KERNEL.currProc].regs = trace;
			trace = KERNEL.processes[next].regs;
			KERNEL.currProc = next;
			
			
			KERNEL.tss.esp0 = KERNEL.processes[KERNEL.currProc].tssAddr;
			if(trace.cs == 0x10)
			{
				trace.ss = KERNEL.processes[KERNEL.currProc].pdAddr;
				trace.int_no = 0;
			}	
			else if(prevCS == 0x10)
			{
				trace.ss = KERNEL.processes[KERNEL.currProc].pdAddr;
				trace.int_no = 1;
			}
			else
			{
				setCR3(KERNEL.processes[KERNEL.currProc].pdAddr);
			}	
		}
	}
	// Send reset signal to master
	outb(0x20, 0x20);
}