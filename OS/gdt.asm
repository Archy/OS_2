section .text
global _getGDTR
global _flushGDT

align 4

_getGDTR:
	push ebx
	mov ebx, [esp+8]

	;load id
	sgdt [ebx]
	mov eax, 0
	mov eax, [ebx+2] ;
	pop ebx
	ret

_flushGDT:
	push ebp
	mov ebp, esp
	push ebx

	mov eax, 0
	;load new idt limit
	mov eax, [ebp+8]
	;limit has size of 16bits
	and eax, 0x0000FFFF
	;load new idt base
	mov ebx, [ebp+12]

	push ebx
	push eax
	;move limit to proper position on stack
	mov [esp+2], word ax
	;load idt
	lgdt [esp + 2]

;flush gdt
    jmp 0x10:flush_gdt
 
flush_gdt:
    mov ax, 0x08
    mov ds, ax
    mov es, ax
    mov fs, ax
    mov gs, ax
    mov ss, ax

	;remove base and limit from stack
	add esp, 8

	pop ebx
	pop ebp
	ret