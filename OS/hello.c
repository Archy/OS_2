#include "gnu-efi/inc/efi.h"
#include "Kernel.h"

//targeted system guard 
#if defined(__linux__)
#error "You are not using a cross-compiler"
#endif
//targeted architecture guard
#if !defined(__i386__)
#error "This needs to be compiled with a ix86-elf compiler"
#endif

extern void init_serial();
//gdt
extern void initGDT();
//interrupts
extern void initInterrupts();
extern void f2Test();
//paging
extern void setIdentityPaging();
extern void enablePaging();
//processes  test
extern void startTest();
//processes 
extern void load(EFI_HANDLE parentHandle);

EFI_STATUS
EFIAPI
efi_main(EFI_HANDLE ImageHandle, EFI_SYSTEM_TABLE *SystemTable)
{
	EFI_SYSTEM_TABLE *ST = SystemTable;
    EFI_STATUS result; 
	
//Store the system table for future use in other functions 
	KERNEL.ST = ST;
	KERNEL.BS = ST->BootServices;
	KERNEL.idtSize = 0x7FF;
	KERNEL.maxProc = 0;
	KERNEL.currProc = KERNEL.maxProc + 1;
	busy = 0;

    result = ST->ConOut->OutputString(ST->ConOut, L"OS starting\n\r");
    if (EFI_ERROR(result))
        return result;
    
    init_serial();
    portString("PORT DEBUG INITALIZED\n");

//gdt, paging and interrupts init
    initInterrupts();
	initGDT();
	
	f2Test();
	setIdentityPaging();
    enablePaging();
	ST->ConOut->OutputString(ST->ConOut, L"paging enabled\n\r");
	
	f2Test();
//setting up processes
	load(ImageHandle);

	startTest();
	waitForUser();
	//shutdown system
	ST->RuntimeServices->ResetSystem(
		EfiResetShutdown,
		result,
		0, NULL  //no reset data
	);
}