extern _outb
extern _systemCall
extern _printHEX
extern _isrHandler
extern _timerCallback

extern _busy

section .text
global _isr0
global _isr1
global _isr2
global _isr3
global _isr4
global _isr5
global _isr6
global _isr7
global _isr8		;***
global _isr9
global _isr10		;***
global _isr11		;***
global _isr12 		;***
global _isr13		;***
global _isr14		;***
global _isr17
global _isr18
global _isr80

global _irq0

align 4

_irq0:
	cli
	push eax
	mov eax, [esp+8]
	cmp eax, 0x1B
	je _fromUserMode 
	;interrupt int kernel mode
	;no ss and esp on stack
	;shift all stack and put ss and esp in the begining
	push dword 0	;place for ss and esp
	push dword 0
	;old eax
	mov eax, [esp+8]
	mov [esp],  eax
	;old eip
	mov eax, [esp+12]
	mov [esp+4],  eax
	;old cs
	mov eax, [esp+16]
	mov [esp+8],  eax
	;old cs
	mov eax, [esp+20]
	mov [esp+12],  eax
	;place old esp
	mov eax, esp
	add eax, 24
	mov [esp+16], eax
	;place ss
	mov eax, 0x08
	mov [esp+20], eax

_fromUserMode:
	pop eax
	
	push byte 0
	push byte 0
	
	pushad                    ; Pushes edi,esi,ebp,esp,ebx,edx,ecx,eax

	mov ax, ds               ; Lower 16-bits of eax = ds.
	push eax                 ; save the data segment descriptor

	mov ax, 0x08  ; load the kernel data segment descriptor
	mov ds, ax
	mov es, ax
	mov fs, ax
	mov gs, ax

	call _timerCallback

	pop ebx        ; reload the original data segment descriptor
	mov ds, bx
	mov es, bx
	mov fs, bx
	mov gs, bx

	popad              	; pop registers
	push eax
	
	mov eax, [esp+4]
	cmp eax, 0
	je _tryShift
	
	;jumping from kernel to user space 
	;requires changing cr3
	pop eax
	add esp, 8     	; clean up the pushed error code
	push eax
	push dword 0 	;for next cr3
	push esi
	push edi
	push ebx
	push ecx
	
	mov eax, [esp+36]
	mov edi, [esp+40]
	mov [esp+16], edi
	mov esi, cr3
	mov ebx, [esp+32]
	mov ecx, [esp+28]
	
	;switch cr3
	mov cr3, edi
	;ss
	sub eax, 4
	mov [eax], dword 0x23
	;esp
	sub eax, 4
	mov [eax], eax
	add [eax], dword 8
	;flags
	sub eax, 4
	mov [eax], ebx
	;cs
	sub eax, 4
	mov [eax], ecx
	
	;switch cr3
	mov cr3, esi
	mov ebx, [esp+24]
	mov ecx, [esp+20]
	
	mov cr3, edi
	;eip
	sub eax, 4
	mov [eax], ebx
	;eax
	sub eax, 4
	mov [eax], ecx
	
	mov cr3, esi
	pop ecx
	pop ebx
	pop edi
	pop esi
	mov esp, [esp]
	
	mov cr3, esp
	mov esp, eax
	jmp _toUserMode

	
_tryShift:
	pop eax
	add esp, 8     	; clean up the pushed error code
	
	push eax
	mov eax, [esp+8]	;next cs
	cmp eax, 0x1B
	je _toUserMode
	mov eax, [esp+20]	;ss
	cmp eax, 0x08			
	jne _changeStacks		;if ss holds new cr3 change stacks
	
;shift stacks
	;flags to old ss
	mov eax, [esp+12]
	mov [esp+20], eax
	;cs to old esp
	mov eax, [esp+8]
	mov [esp+16], eax
	;eip to flags
	mov eax, [esp+4]
	mov [esp+12], eax
	;eax to cs
	mov eax, [esp]
	mov [esp+8], eax
	;pop useless values
	add esp, 8
	jmp _toUserMode
	
_changeStacks:
	mov eax, [esp+16]	;new esp
	push dword 0	;for future cr3
	push ebx
	push ecx
	push edx
	push edi
	push esi
	push ebp
	
	mov ebx, [esp+28]
	mov ecx, [esp+32]
	mov edx, [esp+36]
	mov edi, [esp+40]
	mov esi, [esp+48]
	mov ebp, cr3
	mov [esp+24], esi
	
	;goto new proc memory 
	mov cr3, esi
	; 'push' flags
	sub eax, 4
	mov [eax], edi
	; 'push' cs
	sub eax, 4
	mov [eax], edx
	; 'push' eip
	sub eax, 4
	mov [eax], ecx
	; 'push' eax
	sub eax, 4
	mov [eax], ebx
	
	;goto old proc memory
	mov cr3, ebp
	
	;pop registers
	pop ebp
	pop esi
	pop edi
	pop edx
	pop ecx
	pop ebx
	
	;back to new proc memory
	mov esp, [esp]
	mov cr3,  esp
	mov esp, eax

 _toUserMode:
	
	;set flags to reenable interrupts (safe!)
	mov eax, [esp+12]
	or eax, 0x200
	mov [esp+12], eax
	pop eax
	mov [_busy], dword 0
	iret           		; pops 5 things at once: CS, EIP, EFLAGS (SS, and ESP)

;system call interrupt INT 80H
_isr80:
	cli

	pusha					; push all registers 
	push ds					;remember data segment
	push eax				;load system call number
	mov ax, 8H				;load kernel data segment descriptor
	mov ds, ax

	call _systemCall	;system call handler
	
	add  esp, 4			;remove syscall nr
	pop ds
	popa						; restore all registers 

	sti
	iret							; and return from exception 

;interrupt service routine works as follows:
;first it turns of interruptions (cli), then
;pushes exception nr on stack and jumps to isr_common
;note that some exceptions load error code on stack, 
;which are overwriten by first digit of exception nr
_isr0:
	cli
	;2 digit interrupt nr
	push byte 0
	push byte 0
	jmp isr_common
_isr1:
	cli
	push byte 0
	push byte 1
	jmp isr_common
_isr2:
	cli
	push byte 0
	push byte 2
	jmp isr_common
_isr3:
	cli
	push byte 0
	push byte 3
	jmp isr_common
_isr4:
	cli
	
	push eax
	pushf 
	pop eax
	and eax, 0x0
	push eax
	popf
	pop eax
	iret
	
	
	push byte 0
	push byte 4
	jmp isr_common
_isr5:
	cli
	push byte 0
	push byte 5
	jmp isr_common
_isr6:
	cli
	push byte 0
	push byte 6
	jmp isr_common
_isr7:
	cli
	push byte 0
	push byte 7
	jmp isr_common

_isr8:
	cli
	;overwrite error code on stack
	mov [esp], byte 0
	push byte 8
	jmp isr_common

_isr9:
	cli
	push byte 0
	push byte 9
	jmp isr_common

_isr10:
	cli
	;overwrite error code on stack
	mov [esp], byte 1
	push byte 0
	jmp isr_common
_isr11:
	cli
	;overwrite error code on stack
	mov [esp], byte 1
	push byte 1
	jmp isr_common
_isr12:
	cli
	;overwrite error code on stack
	mov [esp], byte 1
	push byte 2
	jmp isr_common
_isr13:
	cli
	;overwrite error code on stack
	mov [esp], byte 1
	push byte 3
	jmp isr_common
_isr14:
	cli
	
	mov eax, [esp]
	mov ebx, cr2
	jmp _isr14
	
	;overwrite error code on stack
	mov [esp], byte 1
	push byte 0
	jmp isr_common

_isr17:
	cli
	;2 digit interrupt nr
	push byte 1
	push byte 7
	jmp isr_common
_isr18:
	cli
	;2 digit interrupt nr
	push byte 1
	push byte 8
	jmp isr_common

;send exception nr to COM1
isr_common:
	pusha

;call standard print function
	mov ax, ds               ; Lower 16-bits of eax = ds.
	push eax                 ; save the data segment descriptor

	mov ax, 0x08  ; load the kernel data segment descriptor
	mov ds, ax
	mov es, ax
	mov fs, ax
	mov gs, ax

	call _isrHandler

	pop eax        ; reload the original data segment descriptor
	mov ds, ax
	mov es, ax
	mov fs, ax
	mov gs, ax
	
;return
	popa
	add esp, 8
	sti	
	iret