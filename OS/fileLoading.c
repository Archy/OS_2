#include "gnu-efi/inc/efi.h"
#include "Kernel.h"
extern void setCR3(UINT32 pdeAddr);
void loadProcess(UINTN index,  CHAR16 *fileName, UINT32 offset, UINT32 size);
void setProcesPaging(UINTN index, UINT32 startAddr);

void load(EFI_HANDLE parentHandle)
{
	KERNEL.ST->ConOut->OutputString(KERNEL.ST->ConOut, L"\n\nFILE LOADING\n\r");
	
	//load processes to memory
	loadProcess(0, L"EFI\\APPS\\proc1.o", 100, 32);
	loadProcess(1, L"EFI\\APPS\\proc2.o", 100, 32);
	//set processes page directories and tables
	setProcesPaging(0, 0);
	setProcesPaging(1, 0);
	
	KERNEL.ST->ConOut->OutputString(KERNEL.ST->ConOut, L"IMAGES LOADED\n\r");
}
void loadProcess(UINTN index,  CHAR16 *fileName, UINT32 offset ,UINT32 size)
{
	EFI_STATUS efiStatus;
	EFI_GUID sfspGuid =  SIMPLE_FILE_SYSTEM_PROTOCOL;
	EFI_HANDLE* handles = NULL;   
	UINTN handleCount = 0;
	UINTN i = 0;
	EFI_FILE* root = NULL;
	EFI_FILE *file = NULL;
	
//enumerate all FS protocols (page 203)
	efiStatus = KERNEL.BS->LocateHandleBuffer(
		ByProtocol, 	// SearchType
		&sfspGuid,   // Protocol
		NULL, 			// SearchKey
		&handleCount,  // The number of handles returned in Buffer
		&handles);		// A pointer to the buffer (must )
//open  EFI_SIMPLE_FILE_SYSTEM_PROTOCOL for found handle
	EFI_FILE_IO_INTERFACE* fs = NULL;
		efiStatus = KERNEL.BS->HandleProtocol(
			handles[0],
			&sfspGuid,
			(void**)&fs
		);
//open volume
	efiStatus = fs->OpenVolume(fs, &root);
	
//open file
	efiStatus = root->Open(
		root, 										//*This,
		&file,										//**NewHandle
		fileName,								//*FileName
		EFI_FILE_MODE_READ,		//OpenMode
		EFI_FILE_READ_ONLY | EFI_FILE_HIDDEN | EFI_FILE_SYSTEM 	//Attributes
	);
	switch(efiStatus)
	{
	case 	EFI_SUCCESS:
		KERNEL.ST->ConOut->OutputString(KERNEL.ST->ConOut, L"file opened\n\r");
		break;
	case 	EFI_NOT_FOUND:
		KERNEL.ST->ConOut->OutputString(KERNEL.ST->ConOut, L"file not found\n\r");
		break;
	case 	EFI_NO_MEDIA:
		KERNEL.ST->ConOut->OutputString(KERNEL.ST->ConOut, L"no media\n\r");
		break;
	case 	EFI_MEDIA_CHANGED:
		KERNEL.ST->ConOut->OutputString(KERNEL.ST->ConOut, L"device has different medium\n\r");
		break;
	case 	EFI_DEVICE_ERROR:
		KERNEL.ST->ConOut->OutputString(KERNEL.ST->ConOut, L"device error\n\r");
		break;
	default:
		KERNEL.ST->ConOut->OutputString(KERNEL.ST->ConOut, L"fatal error\n\r");
		break;
	}
	
	/*
	//get file size
	EFI_FILE_INFO fileInfo;
	UINTN infoSize = sizeof(EFI_FILE_INFO);
	EFI_GUID fileInfoId = EFI_FILE_INFO_ID;
	
	file->GetInfo(
		file,
		&fileInfoId,
		&infoSize, 
		&fileInfo
	);
	switch(efiStatus)
	{
	case EFI_SUCCESS:
		KERNEL.ST->ConOut->OutputString(KERNEL.ST->ConOut, L"File info:\n\r");
		KERNEL.ST->ConOut->OutputString(KERNEL.ST->ConOut, fileInfo.FileName);
		printINT(fileInfo.Size);
		printINT(fileInfo.PhysicalSize);
		break;
	case EFI_UNSUPPORTED:
		KERNEL.ST->ConOut->OutputString(KERNEL.ST->ConOut, L"InformationType is not known\n\r");
		break;
	case EFI_DEVICE_ERROR:
		KERNEL.ST->ConOut->OutputString(KERNEL.ST->ConOut, L"The device reported an error.\n\r");
		break;
	case EFI_NO_MEDIA:
		KERNEL.ST->ConOut->OutputString(KERNEL.ST->ConOut, L"The device has no medium.\n\r");
		break;
	case EFI_VOLUME_CORRUPTED:
		KERNEL.ST->ConOut->OutputString(KERNEL.ST->ConOut, L"The file system structures are corrupted.\n\r");
		break;
	case EFI_BUFFER_TOO_SMALL:
		KERNEL.ST->ConOut->OutputString(KERNEL.ST->ConOut, L"The BufferSize is too small\n\r");
		break;
	}/**/
	
	UINTN pagesNr = 1;
	UINTN requstedSize;
	void *pagesAddr;
	KERNEL.BS->AllocatePages(AllocateAnyPages, EfiLoaderData, pagesNr, &pagesAddr);
	requstedSize = 4096 - offset;
	
	printHEX(pagesAddr);
	printHEX(requstedSize);
	void *startAddr = pagesAddr + offset ;
	efiStatus = file->Read(
		file,
		&requstedSize,
		startAddr
	);
	switch(efiStatus)
	{
	case EFI_SUCCESS:
		KERNEL.ST->ConOut->OutputString(KERNEL.ST->ConOut, L"Success\n\r");
		printHEX(requstedSize);
		break;
	case EFI_BUFFER_TOO_SMALL:
		KERNEL.ST->ConOut->OutputString(KERNEL.ST->ConOut, L"Buffer to small\n\r");
		break;
	default:
		KERNEL.ST->ConOut->OutputString(KERNEL.ST->ConOut, L"Other error\n\r");
		break;
	}
	
	KERNEL.processes[index].psychAddr = pagesAddr;
	KERNEL.processes[index].fileSize = requstedSize;
//free allocated memory
	file->Close(file);
	KERNEL.BS->FreePool(handles);
}
void setProcesPaging(UINTN index, UINT32 startAddr)
{
//allocate page for page tables directory
	EFI_STATUS result;
	UINT32 i = 0;
	UINT32 pdCount = 0;
	
	EFI_PHYSICAL_ADDRESS pdAddr = 0;
	EFI_PHYSICAL_ADDRESS ptAddr = 0;
	UINT32 pde = 0;
	UINT32 *currPDE = 0;
	UINT32 pte = 0;
	UINT32 *currPTE = 0;
	UINT32 *newPTE = 0;
	
	UINT32 pageSize = 4 * 1024;
	UINT32 pageGroupSize = 4*1024*1024; // page_size * entries per page_table
//allocate page for page direcotry and copy from entries kernel pd
	KERNEL.BS->AllocatePages(AllocateAnyPages, EfiLoaderData, (UINTN)1, &pdAddr);
	KERNEL.processes[index].pdAddr = pdAddr;
	currPDE = pdAddr;	
	memcpy(KERNEL.pageDir, KERNEL.processes[index].pdAddr, pageSize);
	
	currPTE = (*currPDE) & 0xFFFFF000;
	KERNEL.BS->AllocatePages(AllocateAnyPages, EfiLoaderData, (UINTN)1, &ptAddr);
	newPTE = ptAddr;
	memcpy(currPTE, newPTE, pageSize);
	pte = newPTE;
	*currPDE = (pte | 0x0000001F);
	++newPTE; 	//fpage nr 1 (counting from 0)
	*newPTE = (KERNEL.processes[index].psychAddr & 0xFFFFF000) | 0x0000001F;
	
//setup process stack
	EFI_PHYSICAL_ADDRESS espPsychAddr = 0;
	UINT32 esp;
	//set process inital esp value
	//stack is on the second page in process address space
	//esp is at the end of second page
	esp =  3*4096 - 16;
	esp = esp - (esp%16);
	KERNEL.processes[index].regs.esp = esp;
	//allocate and map stack space
	KERNEL.BS->AllocatePages(AllocateAnyPages, EfiLoaderData, (UINTN)1, &espPsychAddr);
	++newPTE;
	*newPTE = (espPsychAddr & 0xFFFFF000) | 0x0000001F;
	
//setup proces tss
	EFI_PHYSICAL_ADDRESS tssPsychAddr = 0;
	KERNEL.BS->AllocatePages(AllocateAnyPages, EfiLoaderData, (UINTN)1, &tssPsychAddr);
	KERNEL.processes[index].tssAddr = tssPsychAddr;
	/*KERNEL.ST->ConOut->OutputString(KERNEL.ST->ConOut, L"IMAGE PD\n\r");
	currPDE = pdAddr;
	for(int i=0; i<1024; ++i, ++currPDE)
	{
		if(*currPDE != 0)
		{
			printHEX(i);
			pde = *currPDE;
			pde =  pde & 0xFFFFF000;
			printHEX(pde);
			UINT32 j = 0;
			currPTE = pde;
			for(j = 0; j<4; ++j, ++currPTE)
			{
				if(*currPTE != 0)
				{
					KERNEL.ST->ConOut->OutputString(KERNEL.ST->ConOut, L"\t");
					printHEX(j);
					pte = *currPTE;
					pte =  pte & 0xFFFFF000;
					KERNEL.ST->ConOut->OutputString(KERNEL.ST->ConOut, L"\t");
					printHEX(pte);
				}
			}
		}
	}/**/
}


