extern _printHEX

section .text
global _outb:function (_outb.end - _outb)
global _getIDT:function (_getIDT_end - _getIDT)
global _loadIDT:function (_loadIDT_end - _loadIDT)
;system calls functions
global _getDS
global _getCS
global _f2Test
;global _isrTest:function (_isrTest_end - _isrTest)

align 4

;int outb(unsigned short port, unsigned char val);
;send value to given port
_outb:
	push ebp
	mov ebp, esp
	push eax
	push edx

	mov eax, [ebp+12]	;val
	mov edx, [ebp+8]	;port
	out dx, al  ;have to be dx and al

	pop edx
	pop eax
	pop ebp
	ret
.end:


;extern void getIDT(IDT_T* idt);
;loads idt base addr and limit to given struct
_getIDT:
	push ebx
	mov ebx, [esp+8]

	;load id
	sidt [ebx]
	mov eax, 0
	mov eax, [ebx+2] ;
	pop ebx
	ret
_getIDT_end:

;extern void loadIDT(IDT_T idt);
;loads new idt from given struct with
;new idt base and limit
_loadIDT:
	push ebp
	mov ebp, esp
	push ebx

	mov eax, 0
	;load new idt limit
	mov eax, [ebp+8]
	;limit has size of 16bits
	and eax, 0x0000FFFF
	;load new idt base
	mov ebx, [ebp+12]

	push ebx
	push eax
	;move limit to proper position on stack
	mov [esp+2], word ax
	;load idt
	lidt [esp + 2]
	;remove base and limit from stack
	add esp, 8

	pop ebx
	pop ebp
	ret
_loadIDT_end:

;#################
;TEST

_getDS:
	mov eax, 0
	mov ax, ds
	ret

_getCS:
	mov eax, 0
	mov ax, cs
	ret
	

_f2Test
	mov eax, 2
	int 0x80
	int 0x2
	ret