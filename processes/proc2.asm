;nasm -f bin proc2.asm -o proc2.o
bits 32
ORG 0x1064

SECTION .text
	;push text on stack
	sub esp, 8
	mov edi, esp
	mov esi, mystr
	mov ecx, mystr_len
	rep movsb
_loop:
	;print text via system call
    mov eax, 2
    int 80H

    mov ecx, 100000
    _wait:
    	push ecx
    	pop ecx
    	loop _wait
    
    jmp _loop
_info:
	;text to print
	mystr db "proc2",0
	mystr_len equ $ - mystr