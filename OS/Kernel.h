#include "gnu-efi/inc/efi.h"
//printing
extern void waitForUser();
extern void printHEX(UINT32 x);
//init serial ports
extern void portString(const CHAR8 *str);
extern void portHEX(UINT32 x);
 //memory
 extern void memset(void *s, int c, UINT32 size);
 extern void memcpy(void *source, void *dest, UINT32 size);

#ifndef KERNEL_H
#define KERNEL_H

struct MemoryMap
{
  UINTN mapSize;
  uint8_t *memoryMapPtr;
  UINTN mapKey;
  UINTN descriptorSize;
  UINT32 descriptorVersion;
  
  UINT32 totalPages;
};

// A struct describing a Task State Segment.
struct tssEntryStruct
{
   UINT32 prev_tss;   // The previous TSS - if we used hardware task switching this would form a linked list.
   UINT32 esp0;       // The stack pointer to load when we change to kernel mode.
   UINT32 ss0;        // The stack segment to load when we change to kernel mode.
   UINT32 esp1;       // everything below here is unusued now.. 
   UINT32 ss1;
   UINT32 esp2;
   UINT32 ss2;
   UINT32 cr3;
   UINT32 eip;
   UINT32 eflags;
   UINT32 eax;
   UINT32 ecx;
   UINT32 edx;
   UINT32 ebx;
   UINT32 esp;
   UINT32 ebp;
   UINT32 esi;
   UINT32 edi;
   UINT32 es;         
   UINT32 cs;        
   UINT32 ss;        
   UINT32 ds;        
   UINT32 fs;       
   UINT32 gs;         
   UINT32 ldt;      
   UINT16 trap;
   UINT16 iomap_base;
} __packed;
 
struct intStackTrace
{
   UINT32 ds;                  													// Data segment selector
   UINT32 edi, esi, ebp, unused, ebx, edx, ecx, eax; 	// Pushed by pusha.
   UINT32 int_no, err_code;    									// Interrupt number and error code (if applicable)
   UINT32 eip, cs, eflags, esp, ss; 						// Pushed by the processor automatically.
};
 
struct ProcessStruct
{
	UINT32 pdAddr;
	UINT32 psychAddr;
	UINTN fileSize;
	
	 struct intStackTrace regs;
	 UINT32 tssAddr;
};
 
struct IDTDescr
{
   uint16_t baseLow; // offset bits 0..15
   uint16_t selector; // a code segment selector in GDT or LDT
   uint8_t zero;      // unused, set to 0
   uint8_t flags; // type and attributes, see below
   uint16_t baseHigh; // offset bits 16..31
};

struct GDTDescr
{
	uint16_t limitLow;
	uint16_t baseLow;
	uint8_t baseMid;
	uint8_t access;
	uint8_t granularity;
	uint8_t baseHigh;
};
 
struct KernelStruct
{
//uefi structs
	EFI_SYSTEM_TABLE *ST;
	EFI_BOOT_SERVICES *BS;
	struct MemoryMap map;
//kernel memory
	UINT32 pageDir;
//IDT
	struct IDTDescr idt[256];
	UINT32 idtSize;
//GDT
	struct GDTDescr gdt[8] ;
	//SELECTOR		USE
	//		0x00			unused
	//		0x08			kernel data
	//		0x10			kernel code
	//		0x18			user code
	//		0x20			user data
	//		0x28			tss
	UINT16 gdtSize;
  struct tssEntryStruct tss;
//processes data
	struct ProcessStruct processes[2];
	UINT32 currProc;
	UINT32 maxProc;
} KERNEL;

UINT32 busy;

#endif //KERNEL_H