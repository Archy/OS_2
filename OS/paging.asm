section .text
global _enablePaging
global _setCR3

align 4

_enablePaging:
 	mov eax, cr0
 	or eax, 0x80000000
 	mov cr0, eax
	ret
.end:

_setCR3:
	mov eax, [esp+4]
	mov cr3, eax
	ret
.end:

