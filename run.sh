#creating FAT image
dd if=/dev/zero of=fat.img bs=1k count=1440
mformat -i fat.img -f 1440 ::
mmd -i fat.img ::/EFI
mmd -i fat.img ::/EFI/BOOT
mcopy -i fat.img BOOTIA32.EFI ::/EFI/BOOT
#copy processes code to fat
mmd -i fat.img ::/EFI/APPS
mcopy -i fat.img proc1.o ::/EFI/APPS
mcopy -i fat.img proc2.o ::/EFI/APPS

#running 
qemu-system-i386 -serial file:serial.log -m 1G -L . -bios OVMF.fd \
	-usb -usbdevice disk:format=raw:fat.img
