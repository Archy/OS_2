#include "gnu-efi/inc/efi.h"
#include "Kernel.h"

typedef struct
{
	CHAR16 limit;
	UINT32 base;
} GDT_T;

extern UINT32 getGDTR(GDT_T *gdtr);
extern UINT32 flushGDT(GDT_T gdtr);
extern void tssFlush();

void gdtSetGate(UINT32 num, UINT32 base, UINT32 limit, UINT8 access, UINT8 gran);
void writeTSS(UINT32 num, UINT16 ss0, UINT32 esp0);

void initGDT()
{
	GDT_T gdtr;
	KERNEL.ST->ConOut->OutputString(KERNEL.ST->ConOut, L"GDT set\n\r");
	gdtr.base = KERNEL.gdt;
	gdtr.limit = KERNEL.gdtSize = 8 * sizeof(struct GDTDescr);
	
	gdtSetGate(0, 0, 0, 0, 0); 										// Null segment
	gdtSetGate(1, 0, 0xFFFFFFFF, 0x92, 0xCF); 		// kernel data segment
	gdtSetGate(2, 0, 0xFFFFFFFF, 0x9A, 0xCF);		// kernel code segment
	gdtSetGate(3, 0, 0xFFFFFFFF, 0xFA, 0xCF); 		// user code segment
	gdtSetGate(4, 0, 0xFFFFFFFF, 0xF2, 0xCF); 		// user  data segment
	
	//setup global tss
	EFI_PHYSICAL_ADDRESS addr = 0;
	UINT32 esp0 = 0;
	KERNEL.BS->AllocatePages(AllocateAnyPages, EfiLoaderData, (UINTN)1, &addr);
	esp0 = addr + 4080;
	writeTSS(5, 0x08, esp0);

	flushGDT(gdtr);
	tssFlush();
}

void gdtSetGate(UINT32 num, UINT32 base, UINT32 limit, UINT8 access, UINT8 gran)
{
	KERNEL.gdt[num].baseLow    = (base & 0xFFFF);
	KERNEL.gdt[num].baseMid = (base >> 16) & 0xFF;
	KERNEL.gdt[num].baseHigh   = (base >> 24) & 0xFF;

	KERNEL.gdt[num].limitLow   = (limit & 0xFFFF);
	KERNEL.gdt[num].granularity = (limit >> 16) & 0x0F;

	KERNEL.gdt[num].granularity |= gran & 0xF0;
	KERNEL.gdt[num].access      = access;
}

void writeTSS(UINT32 num, UINT16 ss0, UINT32 esp0)
{
	UINT32 base = (UINT32) &(KERNEL.tss);
	UINT32 limit = base + sizeof(struct tssEntryStruct);

	// Now, add our TSS descriptor's address to the GDT.
	gdtSetGate(num, base, limit, 0xE9, 0x00);

   // Ensure the descriptor is initially zero.
   memset((void*)&(KERNEL.tss), 0, sizeof(struct tssEntryStruct));

   KERNEL.tss.ss0  = ss0;  // Set the kernel stack segment.
   KERNEL.tss.esp0 = esp0; // Set the kernel stack pointer.

   // setting of last 2 bits defines  the RPL to 3, meaning that this TSS can be used
   // to switch to kernel mode from ring 3.
   KERNEL.tss.cs   = 0x13;
   KERNEL.tss.ss = KERNEL.tss.ds = KERNEL.tss.es = KERNEL.tss.fs = KERNEL.tss.gs = 0x0b;
}