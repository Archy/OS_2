#include "Kernel.h"
#define PORT 0x3f8   /* COM1 */

void waitForUser()
{
	EFI_STATUS Status;
	EFI_INPUT_KEY Key;
	//emty console input buffer to flush any keystrokes entered
	//before this point
	Status = KERNEL.ST->ConIn->Reset(KERNEL.ST->ConIn, FALSE);
	if (EFI_ERROR(Status))
		return;
	//wait for key to become available
	while ((Status = KERNEL.ST->ConIn->ReadKeyStroke(KERNEL.ST->ConIn, &Key)) == EFI_NOT_READY);
}

void printHEX(UINT32 x)
{
	KERNEL.ST->ConOut->OutputString(KERNEL.ST->ConOut, L"0x");
	UINT32 i = 0;
	UINT32 counter = 32 / 4;
	
	while((x & 0xF0000000) == 0)
	{
		x = x  << 4;
		-- counter;
		if(counter == 0)
		{
			KERNEL.ST->ConOut->OutputString(KERNEL.ST->ConOut, L"0");
			break;
		}
	}
	
	for(i=0; i< counter; ++i)
	{
		UINT32 temp = x;
		temp = temp >> 28;
		CHAR8 hex = temp;
		hex &= 0x0F;
		if(10 <= hex && hex <= 15)
	    {
			//outb(PORT, 'A'+hex-10);
			CHAR16 tab[2] = {'A'+hex-10, 0 };
			KERNEL.ST->ConOut->OutputString(KERNEL.ST->ConOut, tab);
		}
		else if(0 <= hex && hex <= 9)
		{
			//outb(PORT, hex + '0');
			CHAR16 tab[2] = {hex + '0', 0 };
			KERNEL.ST->ConOut->OutputString(KERNEL.ST->ConOut, tab);
		}
		else
		{
			KERNEL.ST->ConOut->OutputString(KERNEL.ST->ConOut, L"?");
		}
		x = x << 4;
	}
	KERNEL.ST->ConOut->OutputString(KERNEL.ST->ConOut, L"\n\r");
}


//serial ports init
void init_serial() {
	outb(PORT + 1, 0x00);    // Disable all interrupts
	outb(PORT + 3, 0x80);    // Enable DLAB (set baud rate divisor)
	outb(PORT + 0, 0x03);    // Set divisor to 3 (lo byte) 38400 baud
	outb(PORT + 1, 0x00);    //                  (hi byte)
	outb(PORT + 3, 0x03);    // 8 bits, no parity, one stop bit
	outb(PORT + 2, 0xC7);    // Enable FIFO, clear them, with 14-byte threshold
	outb(PORT + 4, 0x0B);    // IRQs enabled, RTS/DSR set
}

//write string to port
void portString(const CHAR8 *str)
{
	while(*str != 0)
	{
		outb(PORT, *str);
		++str;
	}
}
//write hex nr to port
void portHEX(UINT32 x)
{
	outb(PORT, '0');
	outb(PORT, 'x');
	UINT32 i = 0;
	UINT32 counter = 32 / 4;
	
	while((x & 0xF0000000) == 0)
	{
		break;
		x = x  << 4;
		-- counter;
		if(counter == 0)
		{
			outb(PORT, '0');
			break;
		}
	}
	
	for(i=0; i< counter; ++i)
	{
		UINT32 temp = x;
		temp = temp >> 28;
		CHAR8 hex = temp;
		hex &= 0x0F;
		if(10 <= hex && hex <= 15)
	    {
			outb(PORT, 'A'+hex-10);
		}
		else if(0 <= hex && hex <= 9)
		{
			outb(PORT, hex + '0');
		}
		else
		{
			outb(PORT, '?');
		}
		x = x << 4;
	}
	outb(PORT, '\n');
	outb(PORT, '\r');
}

void memset(void *s, int c, UINT32 size)
{
	UINT32 i;
	CHAR8 *mem = s;
	for(int i=0; i<size; ++i, ++mem)
	{
		*mem = c;
	}
}

void memcpy(void *source, void *dest, UINT32 size)
{
	UINT32 i;
	CHAR8 *d = dest;
	CHAR8 *s = source;
	for(int i=0; i<size; ++i, ++d, ++s)
	{
		*d = *s;
	}
}