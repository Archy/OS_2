#include "gnu-efi/inc/efi.h"
#include "Kernel.h"
//asm function that send byte to port
extern int outb(unsigned short port, unsigned char val);
//test functions
extern UINT32 getDS();
extern UINT32 getCS();
extern void f1Test();
extern void f2Test();
//pic
extern void remapPIC();


//idt addr and limit
typedef struct
{
	CHAR16 limit;
	UINT32 base;
} IDT_T;

//idtr getter, setter
extern UINT32 getIDT(IDT_T *idt);
extern UINT32 loadIDT(IDT_T idt);
//settting idt entry
void setIDTGatePrivelege(UINT8 num, UINT32 base, UINT16 sel, UINT8 flags, UINT8 privilege);
void setIDTGate(UINT8 num, UINT32 base, UINT16 sel, UINT8 flags)
{
	setIDTGatePrivelege(num, base, sel, flags, 0);
}
//interrupts handling functions
extern void isr0();
extern void isr1();
extern void isr2();
extern void isr3();
extern void isr4();
extern void isr5();
extern void isr6();
extern void isr7();
extern void isr8();
extern void isr9();
extern void isr10();
extern void isr11();
extern void isr12();
extern void isr13();
extern void isr14();
extern void isr17();
extern void isr18();
//system call
extern void isr80(UINT32 addr);
// timer
extern void irq0();
extern void initTimer(UINT32 frequency);

void initInterrupts()
{
	asm ( "cli");
	
	IDT_T idt;
	UINT32 base;
	UINT32 i;
	
	base = getIDT(&idt);
	idt.base = base;
	
	//copy current idt to kernel space
	struct IDTDescr *old = idt.base;
	struct IDTDescr *newIDT = KERNEL.idt;
	memcpy((void*) old, (void*) newIDT, idt.limit);

	idt.base = KERNEL.idt;
	idt.limit = KERNEL.idtSize;
	
//setting up interrupts
	setIDTGate(0, (UINT32)isr0 , 0x10, 0x8E);
	setIDTGate(1, (UINT32)isr1 , 0x10, 0x8E);
	setIDTGate(2, (UINT32)isr2 , 0x10, 0x8E);
	setIDTGate(3, (UINT32)isr3 , 0x10, 0x8E);
	setIDTGate(4, (UINT32)isr4 , 0x10, 0x8E);
	setIDTGate(5, (UINT32)isr5 , 0x10, 0x8E);
	setIDTGate(6, (UINT32)isr6 , 0x10, 0x8E);
	setIDTGate(7, (UINT32)isr7 , 0x10, 0x8E);
	setIDTGate(8, (UINT32)isr8 , 0x10, 0x8E);
	setIDTGate(9, (UINT32)isr9 , 0x10, 0x8E);
	setIDTGate(10, (UINT32)isr10 , 0x10, 0x8E);
	setIDTGate(11, (UINT32)isr11 , 0x10, 0x8E);
	setIDTGate(12, (UINT32)isr12 , 0x10, 0x8E);
	setIDTGate(13, (UINT32)isr13 , 0x10, 0x8E);
	setIDTGate(14, (UINT32)isr14 , 0x10, 0x8E);
	setIDTGate(17, (UINT32)isr17 , 0x10, 0x8E);
	setIDTGate(18, (UINT32)isr18 , 0x10, 0x8E);

//set system call interrupt gate
	setIDTGatePrivelege(0x80, (UINT32)isr80, 0X10, 0X8E, 0X60);

//pic and irq
	setIDTGate(32, (UINT32)irq0 , 0x10, 0x8E);
	remapPIC();
	initTimer(50);
	
//loading new idts
	loadIDT(idt);
	asm ( "sti");
	KERNEL.ST->ConOut->OutputString(KERNEL.ST->ConOut, L"\nInterrupts loaded\n\r");
}

void setIDTGatePrivelege(UINT8 num, UINT32 base, UINT16 sel, UINT8 flags, UINT8 privilege)
{
	KERNEL.idt[num].baseLow = base & 0xFFFF;
   KERNEL.idt[num].baseHigh = (base >> 16) & 0xFFFF;

   KERNEL.idt[num].selector     = sel;
   KERNEL.idt[num].zero = 0;
   
   KERNEL.idt[num].flags   = flags | privilege;
}

void systemCall(UINT32 nr)
{
	switch(nr)
	{
	case 1:
		KERNEL.ST->ConOut->OutputString(KERNEL.ST->ConOut, L"syscall 1\n\r");
		break;
	case 2:
		KERNEL.ST->ConOut->OutputString(KERNEL.ST->ConOut, L"\tsyscall 2\n\r");
		break;
	default:
	KERNEL.ST->ConOut->OutputString(KERNEL.ST->ConOut, L"\t\tother syscall\n\r");
		break;
	}
}

void isrHandler(struct intStackTrace trace)
{
	KERNEL.ST->ConOut->OutputString(KERNEL.ST->ConOut, L"Interrupt:\t");
	printHEX(trace.int_no);
}