#include "Kernel.h"

void startTest()
{
	struct intStackTrace init = { .ds = 0x23, .cs = 0x1b, .ss = 0x23, .esp = 0x2FF0};
	struct intStackTrace initKernel = { .ds = 0x08, .cs = 0x10, .ss = 0x08, .esp = 0x2FF0};
	KERNEL.processes[0].regs = init;
	KERNEL.processes[1].regs = initKernel;
	
	KERNEL.processes[0].regs.eip = 4196;
	KERNEL.processes[1].regs.eip = 4196;
	
	//turn on multiprocessing
	KERNEL.ST->ConOut->OutputString(KERNEL.ST->ConOut, L"Starting\n\r");
	KERNEL.maxProc = 2;
	KERNEL.currProc = KERNEL.maxProc + 1;
	while(1);
}